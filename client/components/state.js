export const userInfo = {
    name: 'Sam Li',
    title: 'Full Stack Web Developer',
};

export const experiences = [
    {
        company: 'JMIR Publications',
        title: 'Web Developer',
        date: 'Aug  2017 - PRESENT',
        duty: [
            'Create REST web services and implement restful api with Ajax',
            'Build interactive user interface with angularJS and jQuery',
            'Train new employee to be familiar with project structure'
        ]
    },
    {
        company: 'Web Canada',
        title: 'Web Developer',
        date: 'Oct 2013 - July 2016',
        duty: [
            'Build complex interact UIs with ReactJS',
            'Translate photoshop designs into pixel perfect HTML5/CSS3',
            'Build maintainable and scalable CSS with BEM architecture pattern'
        ]
    }
];

export const projects = [
    {
        name: 'La Promesse',
        type: 'Admin Dashboard',
        steps: [
            'Create SPA with react and react router',
            'Build reusable and extensible react components',
            'Implement Bulma CSS framework with flexbox'
        ]
    },
    {
        name: 'Site Favourite',
        link: 'https://favourite-site.herokuapp.com/',
        type: 'Web Application',
        steps: [
            'Create restful api using Postgresql with ruby on rails',
            'Implement model relationships',
            'Write reusable react components on Rails'
        ]
    },
    {
        name: 'Events Toronto',
        link: 'http://events-toronto.herokuapp.com/',
        type: 'Web Application',
        steps: [
            'Retrieve data from Toronto open source with cron job daily',
            'Implement Google Oauth with authentication',
            'Increase user experience with AngularJS'
        ]
    }
];

export const skills = [
    {
        category: 'Basic',
        techs: [
            {
                name: 'HTML5',
                color: '#f16528'
            },
            {
                name: 'CSS3',
                color: '#0e72b8'
            },
            {
                name: 'JS(ES6)',
                color: '#fadf1c'
            }
        ]
    },
    {
        category: 'CSS Framework',
        techs: [
            {
                name: 'Bootstrap',
                color: '#60408e'
            },
            {
                name: 'Bulma',
                color: '#04d1b2'
            }
        ]
    },
    {
        category: 'JS Framework',
        techs: [
            {
                name: 'ReactJS',
                color: '#60dafb'
            },
            {
                name: 'VueJs',
                color: '#01bf7f'
            },
            {
                name: 'Angular',
                color: '#c40030'
            },
        ]
    },
    {
        category: 'Package Manager',
        techs: [
            {
                name: 'Webpack',
                color: '#1b78c0'
            },
            {
                name: 'npm',
                color: '#cc0000'
            }
        ]
    },
    {
        category: 'Version Control',
        techs: [
            {
                name: 'GitHub',
                color: '#010101'
            },
            {
                name: 'Bitbucket',
                color: '#284870'
            }
        ]
    },
    {
        category: 'Project Management Tools',
        techs: [
            {
                name: 'Asana',
                color: '#fc7865'
            },
            {
                name: 'Trello',
                color: '#017fcb'
            },
            {
                name: 'JIRA',
                color: '#003365'
            }
        ]
    },
    {
        category: 'Backend Framework',
        techs: [
            {
                name: 'Laravel',
                color: '#fb4f3c'
            },
            {
                name: 'Django',
                color: '#0e3e2e'
            },
            {
                name: 'Ruby on Rails',
                color: '#cc0000'
            }
        ]
    },
    {
        category: 'Programming Language',
        techs: [
            {
                name: 'PHP',
                color: '#777ab3'
            },
            {
                name: 'Python',
                color: '#306897'
            }
        ]
    }
];